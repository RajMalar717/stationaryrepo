package com.salesmanagementsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.salesmanagementsystem.model.Product;

public class ProductDao {
	
	private static DbUtils dbUtils = new MySQLConnector();
	
	public static boolean addProduct(Product product) throws ClassNotFoundException, SQLException {
		
		Connection connection = dbUtils.openConnection();
		
		String query = "INSERT INTO product (product_name, product_quantity, original_price, profit, selling_price) VALUES (?,?,?,?,?)";
		
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setString(1, product.getProduct_name());
		statement.setInt(2, product.getProduct_quantity());
		statement.setDouble(3, product.getOriginal_price());
		statement.setDouble(4, product.getProfit());
		statement.setDouble(5, product.getSelling_price());
		
		int result = statement.executeUpdate();
		statement.close();
		connection.close();
		
		return result > 0;
	}

	public static Product getProduct(int product_id) throws ClassNotFoundException, SQLException {
		
		Connection connection = dbUtils.openConnection();
		
		String query = "SELECT * FROM product WHERE product_id = ?";
		
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setInt(1, product_id);
		
		ResultSet rs = statement.executeQuery();
		
		Product product = new Product();
		
		while(rs.next()) {
			
			product.setProduct_id(rs.getInt("product_id"));
			product.setProduct_name(rs.getString("product_name"));
			product.setProduct_quantity(rs.getInt("product_quantity"));
			product.setOriginal_price(rs.getDouble("original_price"));
			product.setProfit(rs.getDouble("profit"));
			product.setSelling_price(rs.getDouble("selling_price"));
		}
		
		statement.close();
		connection.close();
		
		return product;
	}

	public static List<Product> getProducts() throws ClassNotFoundException, SQLException{
		
		Connection connection = dbUtils.openConnection();
		
		Statement statement = connection.createStatement();
		String query = "SELECT * FROM product";
		
		ResultSet rs = statement.executeQuery(query);
		
		List<Product> productList = new ArrayList<Product>();
		
		while(rs.next()) {
			
			Product product = new Product();
			product.setProduct_id(rs.getInt("product_id"));
			product.setProduct_name(rs.getString("product_name"));
			product.setProduct_quantity(rs.getInt("product_quantity"));
			product.setOriginal_price(rs.getDouble("original_price"));
			product.setProfit(rs.getDouble("profit"));
			product.setSelling_price(rs.getDouble("selling_price"));
			
			productList.add(product);
			
		}
		
		statement.close();
		connection.close();
		
		return productList;
	}
	
	public static boolean updateProduct(Product product) throws ClassNotFoundException, SQLException {
		
		Connection connection = dbUtils.openConnection();
		
		String query = "UPDATE product SET product_name = ?, product_quantity = ?, original_price = ?, profit = ?, selling_price = ? WHERE product_id = ? ";
		PreparedStatement statement = connection.prepareStatement(query);
		
		statement.setString(1, product.getProduct_name());
		statement.setInt(2, product.getProduct_quantity());
		statement.setDouble(3, product.getOriginal_price());
		statement.setDouble(4, product.getProfit());
		statement.setDouble(5, product.getSelling_price());
		statement.setInt(6, product.getProduct_id());
		
		int result = statement.executeUpdate();
		
		statement.close();
		connection.close();
		
		return result > 0;
	}
	
	public static boolean deleteProduct(int product_id) throws ClassNotFoundException, SQLException {
		
		Connection connection = dbUtils.openConnection();
		
		String query = "DELETE FROM product WHERE product_id = ?";
		
		PreparedStatement statement = connection.prepareStatement(query);
		
		statement.setInt(1, product_id);
		int result = statement.executeUpdate();
		
		statement.close();
		connection.close();
		
		return result > 0;
	}
	
}
