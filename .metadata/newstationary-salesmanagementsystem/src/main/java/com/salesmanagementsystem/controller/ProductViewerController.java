package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.service.ProductService;

public class ProductViewerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if(action != null && action.equals("all")){
			getProductList(request, response);
		}
		else {
			getProduct(request, response);
		}
		
	}
	
	private void getProductList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProductService service = new ProductService();
		
		try {
			
			List<Product> productList = service.getProducts();
			request.setAttribute("productList", productList);
			} 
		catch (ClassNotFoundException | SQLException e) {

			request.setAttribute("message",e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("product-details.jsp");
		rd.forward(request, response);
		
	}
	
	public void getProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		int product_id = Integer.parseInt(request.getParameter("product_id"));
		
		ProductService service = new ProductService();
		
		try 
		{
			Product product = service.getProduct(product_id);
			request.setAttribute("product", product);
		} 
		
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("search-edit.jsp");
		rd.forward(request, response);
	}
}
