package com.salesmanagementsystem.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.salesmanagementsystem.model.Product;
import com.salesmanagementsystem.service.ProductService;

public class ProductManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String action = request.getParameter("action");
		
		if(action != null && action.equals("add"))
		{
			addProduct(request, response);
		}
		else if(action != null && action.equals("edit")) 
		{
			updateProduct(request, response);
		}
		else if(action !=null && action.equals("delete")) 
		{
			deleteProduct(request, response);
		}
	}

	private void addProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String product_name = request.getParameter("product_name");
		int product_quantity = Integer.parseInt(request.getParameter("product_quantity"));
		double original_price = Double.parseDouble(request.getParameter("original_price"));
		double profit = Double.parseDouble(request.getParameter("profit"));
		double selling_price = Double.parseDouble(request.getParameter("selling_price"));
		
		ProductService service = new ProductService();
		
		Product product = new Product();
		product.setProduct_name(product_name);
		product.setProduct_quantity(product_quantity);
		product.setOriginal_price(original_price);
		product.setProfit(profit);
		product.setSelling_price(selling_price);
		
		try 
		{
			boolean result = service.addProduct(product);
			
			if(result) {
				request.setAttribute("message", "The product is added successfully!");
			}		
			
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("add-product.jsp");
		rd.forward(request, response);
		
	}
	
	private void updateProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int product_id = Integer.parseInt(request.getParameter("product_id"));
		String product_name = request.getParameter("product_name");
		int product_quantity = Integer.parseInt(request.getParameter("product_quantity"));
		double original_price = Double.parseDouble(request.getParameter("original_price"));
		double profit = Double.parseDouble(request.getParameter("profit"));
		double selling_price = Double.parseDouble(request.getParameter("selling_price"));
		
		Product product = new Product(product_id, product_name, product_quantity, original_price, profit, selling_price);
		
		ProductService service = new ProductService();
		try 
		{
			boolean result = service.updateProduct(product);
			if(result) {
				request.setAttribute("message", "The product is updated successfully. Product_id:" + product.getProduct_id());
			}
			else {
				request.setAttribute("message", "The product is failed to updated! Product_id:" + product.getProduct_id());
			}
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("search-edit.jsp");
		rd.forward(request, response);
		
	}
	
	private void deleteProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int product_id = Integer.parseInt(request.getParameter("product_id"));
		
		ProductService service = new ProductService();
		try 
		{
			boolean result = service.deleteProduct(product_id);
			if(result) {
				request.setAttribute("message", "The product is deleted successfully. Product_id:" + product_id);
			}
			else {
				request.setAttribute("message", "The product is failed to deleted! Product_id:" + product_id);
			}
			
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			request.setAttribute("message", e.getMessage());
		}
		
		response.sendRedirect("ProductViewer?action=all");
		
	}	
}
