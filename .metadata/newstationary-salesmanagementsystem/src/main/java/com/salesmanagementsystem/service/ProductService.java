package com.salesmanagementsystem.service;

import java.sql.SQLException;
import java.util.List;

import com.salesmanagementsystem.dao.ProductDao;
import com.salesmanagementsystem.model.Product;

public class ProductService {
	
	public boolean addProduct(Product product) throws ClassNotFoundException, SQLException {
		
		return ProductDao.addProduct(product);
		
	}

	public Product getProduct(int product_id) throws ClassNotFoundException, SQLException {
		
		return ProductDao.getProduct(product_id);
		
	}
	
	public List<Product> getProducts() throws ClassNotFoundException, SQLException{
		
		return ProductDao.getProducts();
	}
	
	public boolean updateProduct(Product product) throws ClassNotFoundException, SQLException {
		
		return ProductDao.updateProduct(product);
	}
	
	public boolean deleteProduct(int product_id) throws ClassNotFoundException, SQLException {
		
		return ProductDao.deleteProduct(product_id);
	}
}
