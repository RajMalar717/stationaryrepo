<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<!DOCTYPE html>
<html lang="en">
<head> 
	<link href= "https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel= "stylesheet">
	<meta charset="ISO-8859-1">
	<title>Stationary Sales Management System</title>
</head>
<body>

<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Stationary Shop Sales Management System</h1>
  <p>Welcome to our page to get done all your requirements</p> 
</div>

<div class="container" style="margin-top:30px">
  <div class="row">
    <div class="col-sm-4">
   
      <h3>Some Links</h3>
      <p>Lorem ipsum dolor sit ame.</p>
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="head-office.jsp">Head Office</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Branch</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Customer</a>
        </li>
        <li class="nav-item">
	      <a class="nav-link" href="#">Customer Order</a>
	    </li>
	    <li class="nav-item">
	      <a class="nav-link" href="#">Purchase Order</a>
	    </li>
      </ul>
      
      <hr class="d-sm-none">
    </div>
    <div class="col-sm-8">
      <h2>TITLE HEADING</h2>
      <h5>Title description, Dec 7, 2017</h5>
      <img src="stationery-products.jpg" class="rounded" alt="stationery products">
      <p>Some text..</p>
      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
      <br>

    </div>
  </div>
</div>

<div class="jumbotron text-center" style="margin-bottom:0">
  <p>Footer</p>
</div>

</body>
</html>